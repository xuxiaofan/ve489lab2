/*
	C socket send file...
*/

#include <stdio.h>	//printf
#include <stdlib.h>	//atoi
#include <unistd.h>	//close
#include <string.h>	//strlen
#include <sys/socket.h>	//socket
#include <arpa/inet.h>	//inet_addr

#define	BUFF_SIZE	1024
#define BUFF_SIZE_LOG	10
#define FILE_NAME_SIZE	512

int main(int argc , char *argv[])
{
	int sock, l;
	long lSize, n, r, i;
	struct sockaddr_in server;
	char buff[BUFF_SIZE + 1], file_name[FILE_NAME_SIZE];
	FILE *fp;

	//Create socket
	sock = socket(AF_INET , SOCK_STREAM , 0);
	if (sock == -1)	printf("Could not create socket");
	puts("Socket created");

	if (argc < 3)
	{
		puts("ip or port missing...");
		return 1;
	}
	server.sin_addr.s_addr = inet_addr(argv[1]);
	server.sin_family = AF_INET;
	server.sin_port = htons(atoi(argv[2]));
 
	//Connect to remote server
	if (connect(sock , (struct sockaddr *)&server , sizeof(server)) < 0)
	{
		perror("connect failed. Error");
		return 1;
	}

	puts("Connected\n");

	bzero(buff, BUFF_SIZE + 1);
	bzero(file_name, FILE_NAME_SIZE);
	if (argc == 3)
	{
		printf("Please insert your file name:\t");
		scanf("%[^\n]%*c", file_name);
	}
	else strcpy(file_name, argv[3]);

	//open file
	fp = fopen(file_name, "r");
	if (!fp)
	{
		perror("open file failed. Error");
		return 1;
	}

	//obtain file size
	fseek(fp, 0, SEEK_END);
	lSize = ftell(fp);
	rewind(fp);
	n = lSize >> BUFF_SIZE_LOG;
	r = lSize % BUFF_SIZE;
	i = 0;

	//send the file
	if (send(sock, file_name, strlen(file_name), 0) < 0)
	{
		puts("Send failed");
		return 1;
	}
	sleep(1);
	while ((l = fread(buff, sizeof(char), BUFF_SIZE, fp)) > 0)
	{
		if (r)	printf("Sending... %li/%li\n", i++, n);
		else	printf("Sending... %li/%li\n", ++i, n);
		if (send(sock, buff, l, 0) < 0)
		{
			puts("Send failed");
			return 1;
		}
		bzero(buff, l);
	}
	fclose(fp);
	close(sock);
	return 0;
}

