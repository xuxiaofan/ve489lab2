/*
	C socket recv file...
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <pthread.h>
#include <time.h>

#define	BUFF_SIZE	1024
#define BUFF_SIZE_LOG	10
#define FILE_NAME_SIZE	512

struct pthread_args
{
	int client_sock;
};

// thread process
void *rec_file(void *arg)
{
	int client_sock, read_size;
	char buff[BUFF_SIZE + 1], file_name[FILE_NAME_SIZE + 1];
	FILE *fp;
	//time_t rawtime;

	pthread_detach(pthread_self());
	client_sock = ((struct pthread_args *) arg) -> client_sock;
	free(arg);
	
	bzero(buff, BUFF_SIZE + 1);
	bzero(file_name, FILE_NAME_SIZE + 1);

	//get file name and open file
	if ( (read_size = recv(client_sock, file_name, FILE_NAME_SIZE, 0)) < 0)
	{
		perror("Server receive file name failed. Error");
		exit(1);
	}
	strcpy(buff, file_name);
	printf("accepting file %s from client %i\n", file_name, client_sock);
	//time(&rawtime);
	//strcat(buff, ctime(&rawtime));
	fp = fopen(buff, "w");
	if (!fp)
	{
		perror("open file failed. Error");
		return NULL;
	}
	bzero(buff, BUFF_SIZE + 1);

	//Receive data from client
	while( (read_size = recv(client_sock, buff, BUFF_SIZE , 0)) > 0)
	{
		fwrite(buff, sizeof(char), read_size, fp);
		bzero(buff, read_size);
	}

	printf("file %s received\n", file_name);
	if(read_size == 0)
	{
		puts("Client disconnected");
		fflush(stdout);
	}
	else if(read_size == -1)
	{
		perror("recv failed");
		return NULL;
	}

	fclose(fp);
	return NULL;
}

int main(int argc , char *argv[])
{
	int socket_desc , client_sock , c;
	struct sockaddr_in server , client;
	struct pthread_args *arg;
	pthread_t tid;
	//char buff[BUFF_SIZE + 1], file_name[FILE_NAME_SIZE + 1];
	//FILE *fp;
	//time_t rawtime;

	//Create socket
	socket_desc = socket(AF_INET , SOCK_STREAM , 0);
	if (socket_desc == -1)
	{
		printf("Could not create socket");
	}
	puts("Socket created");

	//Prepare the sockaddr_in structure
	if (argc < 2)
	{
		puts("port missing...");
		return 1;
	}
	server.sin_family = AF_INET;
	server.sin_addr.s_addr = INADDR_ANY;
	server.sin_port = htons(atoi(argv[1]));

	//Bind
	if( bind(socket_desc,(struct sockaddr *)&server , sizeof(server)) < 0)
	{
		//print the error message
		perror("bind failed. Error");
		return 1;
	}
	puts("bind done");

	//Listen
	listen(socket_desc , 3);

	//Accept and incoming connection
	puts("Waiting for incoming connections...");
	c = sizeof(struct sockaddr_in);

	while(1)
	{
		//accept connection from an incoming client
		client_sock = accept(socket_desc, (struct sockaddr *)&client, (socklen_t*)&c);
		if (client_sock < 0)
		{
			perror("accept failed. Error");
			continue;
		}
		printf("Connection accepted, client socket %i\n", client_sock);
		arg = (struct pthread_args *) malloc(sizeof(struct pthread_args));
		arg->client_sock = client_sock;
		if (pthread_create(&tid, NULL, rec_file, arg))
		{
			perror("create thread failed. Error");
			continue;
		}
		arg = NULL;
	}
	return 0;
}

